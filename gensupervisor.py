conf_str = '''[program:test%s]
directory=/opt/dummyproject
command=/usr/local/bin/python3.5 manage.py writeloop --limit 1000
autostart=false
autorestart=false
redirect_stderr=true
stdout_logfile=/var/log/supervisor/test%s-stdout.log
stderr_logfile=/var/log/supervisor/test%s-stderr.log

'''
conf_group = '''[group:%s]
programs=%s
'''


with open('test.conf', 'w') as f:
    for counter in range(500):
            n = '%s' % counter
            conf = conf_str % (n.zfill(3), n.zfill(3), n.zfill(3))
            f.write(conf)

    counter = 0
    for limit in [300, 30, 70, 100]:
        program_lists = range(counter, counter + limit)
        program_lists = [('%s' % x).zfill(3) for x in program_lists]
        program_lists = [('test%s' % x) for x in program_lists]
        program_lists = ','.join(program_lists)
        conf = conf_group % ('g' + str(counter), program_lists)
        f.write(conf)
        counter += limit
