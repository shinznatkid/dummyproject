#!/usr/bin/python
# -*- coding: utf-8

import logging
import time

from django.core.management.base import BaseCommand
from django.utils import timezone


class Command(BaseCommand):

    def add_arguments(self, parser):
        parser.add_argument('--limit', nargs='?', type=int)

    def handle(self, *args, **options):
        logger = logging.getLogger('django.screen')
        counter = 1
        max_limit = options['limit']
        logger.info('SETTING LIMIT: %s' % (max_limit))
        while True:
            logger.info('LOG %s: %s' % (counter, timezone.now()))
            print('PRINT %s: %s' % (counter, timezone.now()))
            self.stdout.write(self.style.NOTICE('STDOUT %s: %s' % (counter, timezone.now())))
            if max_limit and counter >= max_limit:
                break
            counter += 1
            time.sleep(1)
